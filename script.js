const requestFilms = new XMLHttpRequest();
requestFilms.open('GET', 'https://swapi.co/api/films/');
requestFilms.send();
requestFilms.responseType = 'json';
requestFilms.onload = () => {
    let films = requestFilms.response;
    for (let i = 0; i < films.results.length; i++) {
        let wrapper = document.createElement('div');
        wrapper.className = "wrapper";

        let div1 = document.createElement('div');
        div1.className = "someClass";
        div1.innerHTML = '<p> Episode: ' + films.results[i].episode_id + '</p>' +
            '<p>' + films.results[i].title + '</p>' +
            '<p> Plot: ' + films.results[i].opening_crawl + '</p>';
        wrapper.append(div1);
        let div2 = document.createElement('div');
        div2.className = "characters";
        div2.innerHTML = '<div class="bubbles"> ' +
            '               <div class="bubble"> ' +
            '                       <div class="circleL"></div> ' +
            '               </div> ' +
            '               <div class="bubble"> ' +
            '                       <div class="circleL"></div> ' +
            '               </div> ' +
            '               <div class="bubble"> ' +
            '                       <div class="circleL"></div> ' +
            '               </div> ' +
            '               <div class="bubble"> ' +
            '                       <div class="circleL"></div>' +
            '               </div> ' +
            '             </div>'
        wrapper.append(div2);
        let charactersList = films.results[i].characters;
        let charactersNames = [];
        for (let j = 0; j < charactersList.length; j++) {
            let requestCharacters = new XMLHttpRequest();
            requestCharacters.open('GET', charactersList[j]);
            requestCharacters.responseType = 'json';
            requestCharacters.onload = () => {
                charactersNames.push(requestCharacters.response.name);
                if (charactersNames.length === charactersList.length) {
                    div2.innerHTML = '<p> Characters: ' + charactersNames + '</p>';
                }
            };
            requestCharacters.send();
        }
        document.body.append(wrapper);
    }
};